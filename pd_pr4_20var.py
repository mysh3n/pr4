import argparse


#a

def bold(usr_input):
    usr_input = '<strong>'+usr_input[2:-2]+'</strong>'
    return usr_input
def itallic(usr_input):
    usr_input = '<em>'+usr_input[1:-1]+'</em>'
    return usr_input


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('string', type=str)
    args = parser.parse_args()

    if args.string[:2] == '**' and args.string[-2:] == '**':
        print (bold(args.string))
    elif args.string[:1] == '*' and args.string[-1:] == '*':
        print (itallic(args.string))
    else:
        print("Вы ввели некорректоное значение, попробуйте еще")

if __name__ == '__main__':
    main()